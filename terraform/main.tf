provider "azurerm" {}
provider "azuread" {}

module "accelerator-network" {
  source             = "./accelerator-network"
  project_name       = "${var.project_name}"
  location           = "${var.location}"
  create_new_network = "${var.create_new_network}"
}

locals {
  network = {
    # We have to concat an empty list, because otherwise `element()` fails at terraform plan - element can only operate on a non-empty list.
    # https://github.com/hashicorp/terraform/issues/11210
    new_network      = "${length(module.accelerator-network.aks_vnet_subnet_id) > 0 ? element(concat(module.accelerator-network.aks_vnet_subnet_id, list("")), 0) : ""}"
    existing_network = "${var.existing_subnet_id}"
  }
}

module "accelerator-aks" {
  source                     = "./accelerator-aks"
  project_name               = "${var.project_name}"
  location                   = "${var.location}"
  aks_vnet_subnet_id         = "${var.create_new_network ? local.network["new_network"] : local.network["existing_network"]}"
  service_principal          = "${var.service_principal}"
  service_principal_password = "${var.service_principal_password}"
}

module "accelerator-diagnostics" {
  source                         = "./accelerator-diagnostics"
  aks_cluster_id                 = "${module.accelerator-aks.aks_cluster_id}"
  aks_resource_group_name        = "${module.accelerator-aks.aks_resource_group_name}"
  project_name                   = "${var.project_name}"
  location                       = "${var.location}"
  aks_log_analytics_workspace_id = "${module.accelerator-aks.aks_log_analytics_workspace_id}"
}
