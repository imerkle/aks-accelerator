locals {
  //storage accounts must be lowercase and a-z0-9 only
  storage_account_name = "${replace(lower(var.project_name), "/[^a-z0-9]/", "")}"
}

// Storage Accounts have a public endpoint, and therefore must have a unique FQDN
// Generate a random integer to append to the storage account name
resource "random_integer" "entropy" {
  min = 1
  max = 999
}

/*
Type: Storage Account
Purpose: To provide a Storage Account for diagnostic logs emitted by AKS
*/
resource "azurerm_storage_account" "aks_storage_account" {
  account_replication_type = "LRS"
  account_tier             = "Standard"
  location                 = "${var.location}"
  name                     = "${format("%s%ssa", local.storage_account_name, random_integer.entropy.result)}"
  resource_group_name      = "${var.aks_resource_group_name}"
}

/*
Type: Diagnostic Setting
Purpose: To enable AKS diagnostics
*/
resource "azurerm_monitor_diagnostic_setting" "aks_diagnostics" {
  name                       = "${format("%s-diagnostics", var.project_name)}"
  storage_account_id         = "${azurerm_storage_account.aks_storage_account.id}"
  target_resource_id         = "${var.aks_cluster_id}"
  log_analytics_workspace_id = "${var.aks_log_analytics_workspace_id}"

  log {
    category = "kube-controller-manager"

    retention_policy {
      enabled = false
    }
  }

  log {
    category = "kube-apiserver"

    retention_policy {
      enabled = false
    }
  }

  log {
    category = "kube-scheduler"

    retention_policy {
      enabled = false
    }
  }
}
